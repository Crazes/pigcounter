import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddScoreFormComponent } from './add-score-form.component';

describe('AddScoreFormComponent', () => {
  let component: AddScoreFormComponent;
  let fixture: ComponentFixture<AddScoreFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddScoreFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddScoreFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
