import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {UserService} from '../service/user.service';

@Component({
  selector: 'app-score-add-form',
  templateUrl: './add-score-form.component.html',
  styleUrls: ['./add-score-form.component.scss']
})
export class AddScoreFormComponent implements OnInit {

  public scoreSaved: number;
  scoreSavedInput: FormControl;

  constructor(public userService: UserService) { }

  ngOnInit() {
  }

  addScore(): void {
    this.userService.addToScore(this.scoreSaved);
    this.scoreSaved = null;
  }
  bonJambon(): void {
    this.userService.bonJambon();
  }
}
