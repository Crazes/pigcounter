import { Injectable } from '@angular/core';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  currentUser: User = new User('Jackson');

  constructor() { }

  getCurrentUser(): User {
    return this.currentUser;
  }

  addToScore(score: number): void {
    this.getCurrentUser().score += score;
  }

  bonJambon(): void {
    this.getCurrentUser().score = 0;
  }
}
